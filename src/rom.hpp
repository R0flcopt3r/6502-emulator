#pragma once
#include "const.hpp"
#include <iostream>

struct ROM {
  ROM(WORD &addressbus, BYTE &databus);

  void run();

  WORD &addressbus;
  BYTE &databus;
  BYTE memory[32 * 1024];

  BYTE &operator[](WORD idx);
};
