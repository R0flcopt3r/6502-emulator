#include "rom.hpp"

ROM::ROM(WORD &addressbus, BYTE &databus)
    : addressbus(addressbus), databus(databus){};

void ROM::run() {

  if (this->addressbus >> 15 & 1) {
    databus = this->memory[addressbus & ~(1 << 15)];
  }
}

BYTE &ROM::operator[](WORD idx) { return memory[idx]; }
