#include "cpu.hpp"
#include "common.hpp"
#include "const.hpp"

void CPU::run() {
  std::printf("CPU: 0x%x -- 0x%x | A: 0x%x\n", PC, databus, A);
  if (current_instruction == INS::NOOP ||
      current_instruction == PC_cheat_store) {
    current_instruction = databus;
    PC++;
    return;
  }

  handle_instruction(current_instruction);
  PC++;
}

CPU::CPU(WORD &addressbus, BYTE &databus, bool &read_write)
    : PC(addressbus), databus(databus), read_write(read_write) {
  addressbus = 0x8000;
};

void CPU::handle_instruction(BYTE instruction) {
  switch (instruction) {
  case INS::NOOP:
    break;
  case INS::LDA_IM: {
    set_register(A, databus);
    current_instruction = INS::NOOP;
  } break;
  case INS::LDA_ZP: {
    if (PC == 0xEA) {
      std::cout << "WARN: PC is 0xEA, this might have unintended behavior!"
                << std::endl;
    }
    if (PC_cheat_store == INS::NOOP) { // TODO: 0xEA is valid, so this is bad
      PC_cheat_store = PC;
      PC = databus - 1; // stupid edgecase, PC is always incremented after each
                        // instruction handling
      return;
    }
    A = databus;
    PC = PC_cheat_store;
    PC_cheat_store = current_instruction = INS::NOOP;
    read_write = true;

  } break;
  case INS::STA_ZP: {
    if (PC == 0xEA) {
      std::cout << "WARN: PC is 0xEA, this might have unintended behavior!"
                << std::endl;
    }
    if (PC_cheat_store == INS::NOOP) { // TODO: 0xEA is valid, so this is bad
      PC_cheat_store = PC;
      PC = databus - 1; // stupid edgecase, PC is always incremented after each
                        // instruction handling
      read_write = false;
      databus = A;
      return;
    }
    PC = PC_cheat_store;
    PC_cheat_store = current_instruction = INS::NOOP;
    read_write = true;

  } break;
  case INS::BCC: {
    std::cout << "WARN: BCC doesn't properly emulate the extra cycles when "
                 "branching to new page"
              << std::endl;
    if (PC == INS::NOOP) {
      std::cout << "WARN: PC is 0xEA, this might have unintended behavior!"
                << std::endl;
    }
    if (PC_cheat_store == INS::NOOP) { // TODO: 0xEA is valid, so this is bad
      PC_cheat_store = databus;
      if (common::nth_bit_set(PC_cheat_store, 7)) {
        PC_cheat_store += 0xFF00;
      }
      if (carry) {
        PC_cheat_store = current_instruction = INS::NOOP;
      }
      return;
    }
    PC += PC_cheat_store;
    PC_cheat_store = current_instruction = INS::NOOP;
  } break;
  case INS::CMP_IM: {
    carry = (A >= databus);
    A -= databus;
    zero = (A == 0);
    negative = (common::nth_bit_set(A, 7));
    current_instruction = INS::NOOP;
  } break;
  case INS::ADC_IM: {
    A += databus;
    current_instruction = INS::NOOP;
  } break;
  }
}

void CPU::set_register(BYTE &reg, const BYTE &value) {
  zero = (reg == 0);
  negative = (common::nth_bit_set(reg, 7));
  reg = value;
}
