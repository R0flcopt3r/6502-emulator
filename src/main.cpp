#include "const.hpp"
#include "cpu.hpp"
#include "ram.hpp"
#include "rom.hpp"
#include <future>
#include <iostream>
#include <vector>

int main() {
  WORD addressbus;
  BYTE databus;
  bool read_write = true;
  CPU cpu(addressbus, databus, read_write);
  ROM rom(addressbus, databus);
  RAM ram(addressbus, databus, read_write);

  WORD loc = 0x00;

  rom[loc++] = INS::LDA_IM; // A9
  rom[loc++] = 0x00;
  rom[loc++] = INS::STA_ZP; // 85
  rom[loc++] = 0xFE;
  rom[loc++] = INS::LDA_ZP; // A4 <-- hit skal eg
  rom[loc++] = 0xFE;
  rom[loc++] = INS::ADC_IM; // 69
  rom[loc++] = 0x01;
  rom[loc++] = INS::STA_ZP; // 85
  rom[loc++] = 0xFE;
  rom[loc++] = INS::CMP_IM; // C9
  rom[loc++] = 0x04;
  rom[loc++] = INS::BCC; // B0
  rom[loc++] = ~0x0b + 1;

  for (int cycles = 0; cycles < 70; cycles++) {
    ram.run();
    rom.run();
    cpu.run();
  }

  printf("Zero page: %.2X\n", ram[0x00FE]);
  printf("A register: %.2X\n", cpu.A);
}
