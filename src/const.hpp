#pragma once

using WORD = unsigned short int; // 2 BYTE
using BYTE = unsigned char;      // 1 BYTE

namespace INS {

static const BYTE LDA_ZP = 0xA4; // 3 cycl
static const BYTE LDA_IM = 0xA9; // 2 cycl
static const BYTE LDA_ABS = 0xAD;

static const BYTE LDX_ZP = 0xA6;
static const BYTE LDX_IM = 0xA2;
static const BYTE LDX_ABS = 0xAE;

static const BYTE LDY_ZP = 0xA5;
static const BYTE LDY_IM = 0xA0;
static const BYTE LDY_ABS = 0xAC;

static const BYTE CMP_IM = 0xC9; // 2 cycl

static const BYTE ADC_IM = 0x69; // 2 cycl

static const BYTE STA_ZP = 0x85; // 3 cycl

static const BYTE JSR = 0x20;
static const BYTE BSC = 0xB0;
static const BYTE BCC = 0x90; // 2 + 1 cycl

static const BYTE NOOP = 0xea;
} // namespace INS
