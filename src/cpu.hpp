#pragma once
#include "const.hpp"
#include <iomanip>
#include <iostream>

struct CPU {
  std::string str = "hello ";

  void run();

  CPU(WORD &addressbus, BYTE &databus, bool &read_write);

  WORD &PC; // Program counter, also exposed as address buss
  BYTE &databus;
  BYTE SP;

  BYTE A;
  BYTE Y;
  BYTE X;

  // Flags
  BYTE carry : 1;
  BYTE zero : 1;
  BYTE interrupt_disable : 1;
  BYTE decimal_mode : 1;
  BYTE break_command : 1;
  BYTE overflow : 1;
  BYTE negative : 1;
  bool &read_write; // true means cpu wish to read, false cpu wish to write

  BYTE current_instruction = INS::NOOP;
  WORD PC_cheat_store = INS::NOOP;

  void handle_instruction(BYTE instruction);
  /**
   * sets a register to value. also sets zero and negative flag as appropriate
   *
   * @param reg register to set
   * @param value the value to set
   *
   */
  void set_register(BYTE &reg, const BYTE &value);
};
