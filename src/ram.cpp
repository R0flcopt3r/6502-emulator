#include "ram.hpp"
#include <iostream>

RAM::RAM(WORD &addressbus, BYTE &databus, bool &read_write)
    : addressbus(addressbus), databus(databus), read_write(read_write) {
  for (auto i = 0; i < 2048; i++) {
    memory[i] = 0x0;
  }
}

void RAM::run() {

  if (this->addressbus >> 14 == 0) {
    if (read_write) {
      databus = this->memory[addressbus & ~(1 << 14)];
    } else {
      this->memory[addressbus & ~(1 << 14)] = databus;
    }
  }
}

BYTE &RAM::operator[](WORD idx) { return memory[idx]; }
