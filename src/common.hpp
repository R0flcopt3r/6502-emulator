#pragma once
#include "const.hpp"

namespace common {

/**
 * Checks if the nth bit is set in a byte.
 *
 * @param byte the byte to check
 * @param n the bit to check
 *
 * @returns bool true if the bit is set
 */
bool nth_bit_set(const BYTE &byte, const BYTE &n);
} // namespace common
