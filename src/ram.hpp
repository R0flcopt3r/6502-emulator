#pragma once

#include "const.hpp"

struct RAM {

  void run();
  RAM(WORD &addressbus, BYTE &databus, bool &read_write);

  WORD &addressbus;
  BYTE &databus;
  bool &read_write;

  // 0x3FFF
  BYTE memory[16 * 1024];

  BYTE &operator[](WORD idx);
};
