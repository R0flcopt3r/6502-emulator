#include "common.hpp"

bool common::nth_bit_set(const BYTE &byte, const BYTE &n) {
  if (n > 7 || n < 0) {
    throw "Out of bounds exception for checking nth bit";
  }
  static BYTE mask[] = {128, 64, 32, 16, 8, 4, 2, 1};
  return ((byte & mask[n]) != 0);
}
