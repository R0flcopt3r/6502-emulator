# 6502 emulator

Inspiration: https://www.youtube.com/watch?v=qJgsuQoy9bc - Emulating a CPU in C++ (6502) by Dave Poo

References used:

http://www.obelisk.me.uk/6502/index.html

https://sta.c64.org/cbm64mem.html

https://www.c64-wiki.com/wiki/Main_Page

