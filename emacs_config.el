;;; emacs_config.el --- emacs config -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 R0flcopt3r
;;
;; Author: R0flcopt3r
;; Maintainer: R0flcopt3r
;; Created: February 14, 2021
;; Modified: February 14, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; Emacs specific settings for this project.
;;
;;; Code:

;;; Debug Adapter Protocol template for debugging main
(dap-register-debug-template
  "LLDB :: Start Emulator"
  (list :type "lldb-vscode"
        :cwd (expand-file-name "./")
        :program "./src/emulator"
        :request "launch"
        :name "LLDB :: Start Emulator"
        :preLaunchTask "make -c ./src all"))

(provide 'emacs_config)
;;; emacs_config.el ends here
